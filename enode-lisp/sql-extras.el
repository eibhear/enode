
;; Copyright 2013 Éibhear Ó hAnluain

;; This file is part of ENODE.
;;
;; ENODE is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; ENODE is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with ENODE.  If not, see <http://www.gnu.org/licenses/>.

(defun sql-area-add-table (table-name)
  "A simple function to add a new table to the SQL file."
  (interactive "sTable name: ")
  
  (let ((table-name (upcase table-name)))
    (if (eq major-mode 'sql-mode)
        (if (progn
              (goto-char (point-min))
              (re-search-forward (format "^-- Table: %s$" table-name) nil t))
            (error "Table %s is already there" table-name)
          (goto-char (point-max))
          (goto-char (re-search-backward "^-- Table: .+"))
          (insert (format "-- Table: %s\n\n" table-name))
          (insert (format "desc %s\n\n" table-name))
          (insert (format "select count(*) %s\nfrom   %s;\n\n"
                          table-name table-name))
          )
      (error "Are you in an sql-mode buffer?")
      )
    )
  )

(provide 'sql-extras)
